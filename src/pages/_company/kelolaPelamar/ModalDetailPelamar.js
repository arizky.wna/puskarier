import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Alert, Button, Icon, Modal } from 'rsuite';
import CardExp from '../../../components/card-exp/CardExp';
import { apiGetWithAuth } from '../../../misc/config';

const getAge = dateString => {
  const today = new Date();
  const birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
};

const ModalDetailPelamar = ({ isOpen, close, id }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [profile, setProfile] = useState(null);

  useEffect(() => {
    setIsLoading(true);
    let mounted = true;
    if (id) {
      apiGetWithAuth(`users/${id}`)
        .then(r => {
          if (mounted) {
            setProfile(r.data.data);
            setIsLoading(false);
          }
        })
        .catch(err => {
          Alert.error(err.response ? err.response.data.message : err.name);
          setIsLoading(false);
        });
    }

    return () => {
      mounted = false;
    };
  }, [id]);
  return (
    <Modal size="md" show={isOpen} onHide={close}>
      <Modal.Header>
        <Modal.Title>Detail Pelamar</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="pelamar-detail">
          {isLoading && 'loading...'}
          {!isLoading && profile && (
            <div className="left">
              <div className="profile">
                <div
                  style={{
                    width: 130,
                    height: 130,
                    border: 'none',
                    borderRadius: '50%',
                  }}
                >
                  <img
                    src={profile.job_seeker.profile_picture_url}
                    className="img-wrapper"
                    style={{ borderRadius: '50%' }}
                    width="100%"
                    height="100%"
                    alt=""
                  />
                </div>
                <div className="second">
                  <h6 className="title">
                    {profile.fullname ? profile.fullname.toUpperCase() : ''}
                  </h6>
                  <p>
                    {profile.job_seeker.job
                      ? profile.job_seeker.job.toUpperCase()
                      : 'JOB SEEKER'}
                  </p>
                  <h6>Nomor telepon</h6>
                  <p>{profile.job_seeker.phone || '-'}</p>
                  <h6>Email</h6>
                  <p>{profile.email}</p>
                  <h6>Alamat domisili</h6>
                  <p>{profile.job_seeker.city || '-'}</p>
                </div>
                <div className="third">
                  <h6>Usia</h6>
                  <p>{getAge(profile.job_seeker.date_of_birth)} tahun</p>
                  <h6>Jenis Kelamin</h6>
                  <p>
                    {profile.job_seeker.is_male ? 'Laki-laki' : 'Perempuan'}
                  </p>
                  <h6>Institusi Pendidikan</h6>
                  <p>{profile.job_seeker.institution}</p>
                </div>
              </div>
              <div className="detail">
                <div className="card-detail">
                  <h2 className="title">Tentang Saya</h2>
                  <p className="aboutme">{profile.job_seeker.about}</p>
                </div>
                <div className="card-detail">
                  <div className="title">Pengalaman Kerja</div>

                  {profile.experiences
                    .filter(obj => {
                      return obj.type === 'Pengalaman Kerja';
                    })
                    .map((dpk, i) => (
                      <CardExp
                        exp={dpk}
                        key={i}
                        editable={false}
                        // updateProfile={updateProfile}
                        one={dpk.title}
                        two={dpk.sub_title}
                        three={`${moment(dpk.date_in).format(
                          'MMMM YYYY'
                        )} - ${moment(dpk.date_out).format('MMMM YYYY')} (${
                          dpk.total_months
                        } bulan)`}
                        data={{
                          title: 'Posisi - Status',
                          sub_title: 'Perusahaan',
                          date_in: 'Tanggal Mulai',
                          date_out: 'Tanggal Berakhir',
                        }}
                        className="mb-4"
                      />
                    ))}
                </div>

                <div className="card-detail">
                  <div className="title">Pendidikan</div>
                  {profile.experiences
                    .filter(obj => {
                      return obj.type === 'Pendidikan';
                    })
                    .map((dpk, i) => (
                      <CardExp
                        key={i}
                        exp={dpk}
                        editable={false}
                        // updateProfile={updateProfile}
                        one={dpk.title}
                        two={dpk.sub_title}
                        three={`${moment(dpk.date_in).format(
                          'MMMM YYYY'
                        )} - ${moment(dpk.date_out).format('MMMM YYYY')} (${(
                          dpk.total_months / 12
                        ).toFixed(1)} tahun)`}
                        data={{
                          title: 'Nama Institusi',
                          sub_title: 'Bidang Studi - gelar',
                          date_in: 'Tanggal Mulai',
                          date_out: 'Tanggal Berakhir',
                        }}
                        className="mb-4"
                      />
                    ))}
                </div>

                <div className="card-detail">
                  <h2 className="title">Skills</h2>
                  {profile.job_seeker.skills
                    ? profile.job_seeker.skills.map((d, i) => (
                        <div className="badge" key={i}>
                          {d}
                        </div>
                      ))
                    : ''}
                </div>
                <div className="card-detail">
                  <h2 className="title">CV dan Portfolio</h2>
                  <div className="cv">
                    <span>CV</span>
                    {profile.job_seeker.cv_url ? (
                      <a
                        href={profile.job_seeker.cv_url}
                        style={{ fontSize: 13 }}
                        target="blank"
                      >
                        <Icon
                          icon="file-text-o"
                          size="lg"
                          style={{ color: 'blue', marginRight: 10 }}
                        />
                        {profile.job_seeker.cv_name}
                      </a>
                    ) : (
                      'Tidak ada'
                    )}
                  </div>

                  <div className="cv">
                    <span>Portfolio</span>
                    {profile.job_seeker.portfolio_url ? (
                      <a
                        href={profile.job_seeker.portfolio_url}
                        target="blank"
                        style={{ fontSize: 13 }}
                      >
                        <Icon
                          icon="file-text-o"
                          size="lg"
                          style={{ color: 'blue', marginRight: 10 }}
                        />
                        {profile.job_seeker.portfolio_name}
                      </a>
                    ) : (
                      'Tidak ada'
                    )}
                  </div>
                </div>
                <div className="card-detail">
                  <h2 className="title">Pengalaman Organsisasi</h2>
                  {profile.experiences
                    .filter(obj => {
                      return obj.type === 'Pengalaman Organisasi';
                    })
                    .map((dpk, i) => (
                      <CardExp
                        key={i}
                        exp={dpk}
                        editable={false}
                        // updateProfile={updateProfile}
                        one={dpk.title}
                        two={dpk.sub_title}
                        three={`${moment(dpk.date_in).format(
                          'MMMM YYYY'
                        )} - ${moment(dpk.date_out).format('MMMM YYYY')} (${
                          dpk.total_months
                        } bulan)`}
                        data={{
                          title: 'Nama Organisasi / Relawan',
                          sub_title: 'Jabatan Organisasi',
                          date_in: 'Tanggal Mulai',
                          date_out: 'Tanggal Berakhir',
                        }}
                        className="mb-4"
                      />
                    ))}
                </div>

                <div className="card-detail">
                  <h2 className="title">Prestasi</h2>
                  {profile.experiences
                    .filter(obj => {
                      return obj.type === 'Prestasi';
                    })
                    .map((dpk, i) => (
                      <CardExp
                        key={i}
                        exp={dpk}
                        editable={false}
                        // updateProfile={updateProfile}
                        one={dpk.title}
                        two={dpk.sub_title}
                        three={`${moment(dpk.date_in).format('YYYY')}`}
                        data={{
                          title: 'Nama Penghargaan',
                          sub_title: 'Penyelenggara',
                          date_in: 'Tahun',
                        }}
                        className="mb-4"
                      />
                    ))}
                </div>
                <div className="card-detail">
                  <h2 className="title">Pengalaman Pelatihan</h2>
                  {profile.experiences
                    .filter(obj => {
                      return obj.type === 'Pengalaman Pelatihan';
                    })
                    .map((dpk, i) => (
                      <CardExp
                        key={i}
                        exp={dpk}
                        editable={false}
                        // updateProfile={updateProfile}
                        one={dpk.title}
                        two={dpk.sub_title}
                        three={`${moment(dpk.date_in).format('YYYY')}`}
                        data={{
                          title: 'Nama Pelatihan',
                          sub_title: 'Penyelenggara',
                          date_in: 'Tahun',
                        }}
                        className="mb-4"
                      />
                    ))}
                </div>
              </div>
            </div>
          )}
        </div>
      </Modal.Body>
      <Modal.Footer style={{ padding: '20px 20px' }}>
        <Button onClick={close} appearance="primary">
          Ok
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ModalDetailPelamar;
