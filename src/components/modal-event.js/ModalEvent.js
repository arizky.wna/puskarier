import React from 'react';
import { Button, Modal } from 'rsuite';
import deleteIcon from '../../assets/img/deleteIcon.png';
import './modal-event.style.scss';

const ModalEvent = ({ isOpen, close, onClick, isLoading }) => {
  return (
    <Modal
      className="modalevent"
      backdrop="static"
      show={isOpen}
      onHide={close}
      size="xs"
    >
      <Modal.Body className="body">
        <img src={deleteIcon} alt="" />
        <h4>Apakah Anda yakin ingin membatalkan pendaftaran event?</h4>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={close} appearance="subtle">
          Batal
        </Button>
        <Button
          onClick={onClick}
          appearance="primary"
          className="btn-primary2"
          loading={isLoading}
        >
          Ya
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ModalEvent;
