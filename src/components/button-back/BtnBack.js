import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from 'rsuite';

const BtnBack = () => {
  const history = useHistory();
  return <Button onClick={history.goBack}>back</Button>;
};

export default BtnBack;
